#include "types.h"
#include "stdint.h"
#include "stdbool.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "tm4c123gh6pm.h"

void PortsInit (void);
void PinsInit (void);
void TimerInit0 (void);
void TimerInit1 (void);
void InterruptInit (void);

void Timer0Handler(void);
void Timer1Handler(void);
void SwitchHandler(void);