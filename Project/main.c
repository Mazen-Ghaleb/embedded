#include "Traffic.h"

void main()
{
  TrafficInit();                // Initialize peripherals
  TrafficStart();               // Initialize LED states
  
  TrafficDelay(1000);           // Trigger the traffic timer
  while(1) __asm("wfi\n");      // Interrupts do the rest
}
