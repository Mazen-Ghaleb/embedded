#include "Inits.h"

void PortsInit (void){
  // Enable PORTF and wait for it to be ready
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF)) {}
  
  // Enable PORTD and wait for it to be ready
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
  while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)) {}
  
  // Enable PORTE and wait for it to be ready
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
  while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)) {}
}

void PinsInit (void){
  // Unlock PORTF
  HWREG(GPIO_PORTF_BASE+GPIO_O_LOCK) = GPIO_LOCK_KEY;
  HWREG(GPIO_PORTF_BASE+GPIO_O_CR) = GPIO_PIN_0 | GPIO_PIN_4;
  
  // Set the two built-in switches to be input pins
  GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_4);
  GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_4, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
  
  // Set PORTD pins 0,1,2,3,6 to be output pins
  GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6);
  // PIN 0 GREEN Normal TRAFFIC 1
  // PIN 1 YELLOW Normal TRAFFIC 1
  // PIN 2 RED Normal TRAFFIC 1
  // PIN 3 Pedestrian GREEN TRAFFIC 1
  // PIN 6 Pedestrian RED TRAFFIC 1
  
  // Set PORTE pins 1,2,3,4,5 to be output pins
  GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5);
  // PIN 1 GREEN Normal TRAFFIC 2
  // PIN 2 YELLOW Normal TRAFFIC 2
  // PIN 3 RED Normal TRAFFIC 2
  // PIN 4 Pedestrian GREEN TRAFFIC 2
  // PIN 5 Pedestrian RED TRAFFIC 2
}

void TimerInit0 (void){
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);         // Enable timer 0
  while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0)){} // Wait for it to be ready
  TimerDisable(TIMER0_BASE, TIMER_A);                   // Disable it until it is enabled later on
  
  TimerIntDisable(TIMER0_BASE,TIMER_TIMA_TIMEOUT);      // Sandwich
  TimerConfigure(TIMER0_BASE, (TIMER_CFG_PERIODIC));    // Make it a periodic timer
  TimerIntClear(TIMER0_BASE,TIMER_TIMA_TIMEOUT);        // Clear any existing interrupts
  IntPrioritySet(INT_TIMER0A , 0x40);                   // Set priority to 2
  TimerIntRegister(TIMER0_BASE,TIMER_A,Timer0Handler);  // Register the interrupt handler
  TimerIntEnable(TIMER0_BASE,TIMER_TIMA_TIMEOUT);       // Sandwich
}

void TimerInit1 (void){
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);         // Enable timer 1
  while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1)){} // Wait for it to be ready
  TimerDisable(TIMER1_BASE, TIMER_A);                   // Disable it until it is enabled later on
  
  TimerIntDisable(TIMER1_BASE,TIMER_TIMA_TIMEOUT);      // Sandwich
  TimerConfigure(TIMER1_BASE, (TIMER_CFG_PERIODIC));    // Make it a periodic timer
  TimerIntClear(TIMER1_BASE,TIMER_TIMA_TIMEOUT);        // Clear any existing interrupts
  IntPrioritySet(INT_TIMER1A , 0x20);                   // Set priority to 1
  TimerIntRegister(TIMER1_BASE,TIMER_A,Timer1Handler);  // Register the interrupt handler
  TimerIntEnable(TIMER1_BASE,TIMER_TIMA_TIMEOUT);       // Sandwich
}

void InterruptInit(void){
  GPIOIntDisable(GPIO_PORTF_BASE,GPIO_PIN_4|GPIO_PIN_0);        // Sandwich
  GPIOIntClear(GPIO_PORTF_BASE,GPIO_PIN_4|GPIO_PIN_0);          // Clear any existing interrupts
  IntPrioritySet(INT_GPIOF , 0x00);                             // Set priority to 0
  GPIOIntRegister(GPIO_PORTF_BASE,SwitchHandler);               // Register the interrupt handler
  GPIOIntTypeSet(GPIO_PORTF_BASE,GPIO_PIN_4|GPIO_PIN_0,GPIO_FALLING_EDGE); // Set the switches to trigger once pressed (since the switches are configured as Pull-down)
  GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4|GPIO_PIN_0);        // Sandwich
}



