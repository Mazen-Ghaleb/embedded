#include "Traffic.h"
static bool trafficlight = false ;      // Set to false for traffic 1 and true for traffic 2
static bool GreenFlag = true;           // Set to true if a traffic green light is turned on

static int state = 0;                   // The state of the traffic lights:
                                        //      (0 to 4) : Green light
                                        //      (5 to 6) : Yellow light
                                        //      (7 only) : Red light

static int state1 = -1;                 // The state of the pedestrian lights
                                        //     (-1 only) : Red light, and the button is activated immediately
                                        //      (0 to 1) : Green light
                                        //      (2 only) : Red light, and the button is activated once the second is over

static bool pedestrian_spamming = false;// Set to true if the pedestrain pressed the button at state1 = 2

// Converts milliseconds to timer/CPU ticks
uint32 CalcTicks(uint32 milliseconds){
  return ((milliseconds) * (16 * 1000))-1; 
}

// Initializes all the needed peripheralss 
void TrafficInit (void){
  PortsInit ();
  PinsInit ();
  TimerInit0 ();
  TimerInit1 ();
  InterruptInit ();
}

// Starts the traffic timer and sets it to wait for the given milliseconds
void TrafficDelay(uint32 milliseconds){
  TimerDisable(TIMER0_BASE, TIMER_A);                           // Sandwich
  TimerLoadSet(TIMER0_BASE, TIMER_A, CalcTicks(milliseconds));  // Load the ticks value
  TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);               // Clear any existing interrupts
  TimerEnable(TIMER0_BASE, TIMER_A);                            // Sandwich
}

// Starts the pedestrian timer and sets it to wait for the given milliseconds
void PedestrianTrafficDelay(uint32 milliseconds){
  TimerDisable(TIMER1_BASE, TIMER_A);                           // Sandwich
  TimerLoadSet(TIMER1_BASE, TIMER_A, CalcTicks(milliseconds));  // Load the ticks value
  TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);               // Clear any existing interrupts
  TimerEnable(TIMER1_BASE, TIMER_A);                            // Sandwich
}

// Sets the initial state of all the lights
void TrafficStart(void)
{
  GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6,GPIO_PIN_0|GPIO_PIN_6);
  GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_3|GPIO_PIN_4);
}

// Normal Traffic light timer handler
void Timer0Handler(void){
  state = (state + 1) % 8; // Increment the state of the traffic light
  switch (state){
  case 0: // Turn GREEN for other traffic light
    GreenFlag = true;
    if (trafficlight)   // Turn RED for Pedistrian traffic 2, and Turn Normal Traffic 2 to GREEN
      GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_1|GPIO_PIN_5); 
    else                // Turn RED for Pedistrian traffic 1, and Turn Normal Traffic 1 to GREEN
      GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6,GPIO_PIN_0|GPIO_PIN_6); 
    break;
  case 5: // Turn YELLOW
    GreenFlag = false;
    if (trafficlight)   // Turn Normal Traffic 2 to YELLOW
      GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_2|GPIO_PIN_5);
    else                // Turn Normal Traffic 1 to YELLOW
      GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6,GPIO_PIN_1|GPIO_PIN_6);
    break;
  case 7: // Turn RED
    if (trafficlight)   // Turn GREEN for Pedistrian traffic 2, and Turn Normal Traffic 2 to RED
      GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_3|GPIO_PIN_4);
    else               // Turn GREEN for Pedistrian traffic 2, and Turn Normal Traffic 2 to RED
      GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6,GPIO_PIN_2|GPIO_PIN_3);
    trafficlight = !trafficlight; // Next time, perform actions on the other traffic light
    break;
  default: // Stay at the same state
    break;
  }
    // Clear the Timer Interrupt
  TimerIntClear(TIMER0_BASE,TIMER_TIMA_TIMEOUT);
};

// Pedestrian Traffic Light timer Handler
void Timer1Handler(void){
  state1++;
  
  if(state1 == 2) // The pedestrian's passing time is over
  {
    if(trafficlight)    // Turn RED for Pedistrian traffic 2, and Turn Normal Traffic to GREEN
      GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_1|GPIO_PIN_5);
    else                // Turn RED for Pedistrian traffic 1, and Turn Normal Traffic to GREEN
      GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6,GPIO_PIN_0|GPIO_PIN_6);
    
    TimerEnable(TIMER0_BASE, TIMER_A); // Let the traffic timer continue
  }
  else if(state1 == 3)  // One extra second until the button can be pressed again
  {
    if(pedestrian_spamming && GreenFlag) // If the light is currently green, and the pedestrian pressed the button in the last second
    {
      // Reset our state, and disable the traffic timer once again
      state1 = 0;
      TimerDisable(TIMER0_BASE, TIMER_A);
      
      // Turn off the traffic light
      if(trafficlight)  // Turn GREEN for Pedistrian traffic 2, and Turn Normal Traffic to RED
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_3|GPIO_PIN_4);
      else              // Turn GREEN for Pedistrian traffic 1, and Turn Normal Traffic to RED
        GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6,GPIO_PIN_2|GPIO_PIN_3);
    }
    else                                // Nothing needs to be done: set our state to -1 (can be activated immediately) and disable ourselves
    {
      state1 = -1;
      TimerDisable(TIMER1_BASE, TIMER_A);
    }
    pedestrian_spamming = false; // Make sure we reset the flag's stale value
  }
  // Clear the Timer Interrupt
  TimerIntClear(TIMER1_BASE,TIMER_TIMA_TIMEOUT);
}

// Port F Handler , acts as Normal Traffic
void SwitchHandler(void) {
  if(GreenFlag && (state1 == -1 || state1 == 2)) // If a traffic light is GREEN, and the pedestrian button can be pressed
  {
    uint32 switches = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_0|GPIO_PIN_4); // Read the switches
    
    if (trafficlight && (switches & 0x10) == 0x10) // If traffic 2 is GREEN and switch 2 has been pressed
    {
      if(state1 == 2) pedestrian_spamming = true; // If the button can activated after the second is over, just set the flag
      else // If the button can be activated immediately, activate it now
      {
        // Reset the pedestrian state, and disable the traffic timer
        state1 = 0;
        TimerDisable(TIMER0_BASE, TIMER_A);
        
        // Turn Green for Pedistrian traffic 2, and Turn Normal Traffic to RED
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5,GPIO_PIN_3|GPIO_PIN_4);
        
        // Start the pedestrain timer
        PedestrianTrafficDelay(1000);
      }
    }
    if (!trafficlight && (switches & 0x1) == 0x1) {  
      if(state1 == 2) pedestrian_spamming = true; // If the button can activated after the second is over, just set the flag
      else // If the button can be activated immediately, activate it now
      {
        // Reset the pedestrian state, and disable the traffic timer
        state1 = 0;
        TimerDisable(TIMER0_BASE, TIMER_A);
        
        // Turn Green for Pedistrian traffic 1, and Turn Normal Traffic to RED
        GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6,GPIO_PIN_2|GPIO_PIN_3);
        
        // Start the pedestrain timer
        PedestrianTrafficDelay(1000);
      }
    }
  }
    // Clear the Interrupt
  GPIOIntClear(GPIO_PORTF_BASE,GPIO_PIN_0|GPIO_PIN_4);
};
