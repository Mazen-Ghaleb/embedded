var _inits_8h =
[
    [ "InterruptInit", "_inits_8h.html#a5139e9f8579c81621b9886cced6dedab", null ],
    [ "PinsInit", "_inits_8h.html#a6655b21db6ca6e411076f0436770ef4e", null ],
    [ "PortsInit", "_inits_8h.html#a262818491cabe93389170ea942205ba5", null ],
    [ "SwitchHandler", "_inits_8h.html#ad72ccb735680d193dcdbde880534749d", null ],
    [ "Timer0Handler", "_inits_8h.html#abc74d1bf444effc85e5f1090bcc0c017", null ],
    [ "Timer1Handler", "_inits_8h.html#abed1632eba2afda564c2bdd7fb77ee2e", null ],
    [ "TimerInit0", "_inits_8h.html#ade5717c7b14d560cb1a324da7e0f8455", null ],
    [ "TimerInit1", "_inits_8h.html#afed88bd9dd9f3da088078b3f5b2f3f7c", null ]
];