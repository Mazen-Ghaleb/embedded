/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Embedded Systems Project", "index.html", [
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_inits_8c.html",
"tm4c123gh6pm_8h.html#a065131eff8db20748b4af5191d444f36",
"tm4c123gh6pm_8h.html#a10435bf7992a7474b1ca3f0b88e929a0",
"tm4c123gh6pm_8h.html#a1ae44a45c7cffe616f5a9f51bb9fac31",
"tm4c123gh6pm_8h.html#a24fce4d702e7416dac69a5ceb31b8e06",
"tm4c123gh6pm_8h.html#a2e45b5eab432b4cd228d6be80422fd1c",
"tm4c123gh6pm_8h.html#a373641dadf43acfb0a1b27ac00421187",
"tm4c123gh6pm_8h.html#a407304c034a3b9ab9bf07d06497ed443",
"tm4c123gh6pm_8h.html#a4b48760ed93178c9fb9a075015c329f7",
"tm4c123gh6pm_8h.html#a54bc3ef31648d1373539d149e95d4303",
"tm4c123gh6pm_8h.html#a5e9d77025d5aeebc68caf9ca5acd8b92",
"tm4c123gh6pm_8h.html#a685e38eade1ebeccb318600b3ffc5d8b",
"tm4c123gh6pm_8h.html#a71613174169b9dce98567426020e5dd2",
"tm4c123gh6pm_8h.html#a7a5fc4966cdbcba13764a4ababd13c2b",
"tm4c123gh6pm_8h.html#a84c856190bf291c358f0b1f8a3cb149f",
"tm4c123gh6pm_8h.html#a8e4a5ad44767d68149101514ef6a2c41",
"tm4c123gh6pm_8h.html#a982b84be7a1d6d39b6e0b47e58836c4a",
"tm4c123gh6pm_8h.html#aa1831e1514f4bee868ef2a2c867712e3",
"tm4c123gh6pm_8h.html#aab3c4b80a2b83c6a405254cf14832910",
"tm4c123gh6pm_8h.html#ab4cc180998378ed15731bb5acc06f01c",
"tm4c123gh6pm_8h.html#abe6e6dd27dcd61c9a14c8832589d4ac2",
"tm4c123gh6pm_8h.html#ac7186ad3e0888f9e67ac48012d36dcfc",
"tm4c123gh6pm_8h.html#ad150410ab455797dff8bcb538bfda6d7",
"tm4c123gh6pm_8h.html#adafd5881209410c67d4e7891423a3552",
"tm4c123gh6pm_8h.html#ae41c30b33497295d38054f9575f701e4",
"tm4c123gh6pm_8h.html#aedcfb30e25b838b940a37add4adf4ac7",
"tm4c123gh6pm_8h.html#af8221c001a28dc2392fb61873a2b80a3"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';