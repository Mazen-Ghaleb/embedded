var indexSectionsWithContent =
{
  0: "_acefghimnpqrstuw",
  1: "u",
  2: "imst",
  3: "_cimprst",
  4: "ipu",
  5: "fiu",
  6: "acefghinpqstuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

